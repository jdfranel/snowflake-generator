// Snowflake particle class. Original code from :
//
// Coding Challenge 127: Brownian Motion Snowflake
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/127-brownian-snowflake.html
// https://youtu.be/XUA8UREROYE

import Victor = require('victor');

export class Particle {

    private static randomInRange(min: number, max: number) {
        const diff = max - min;
        return min + (Math.random() * diff);
    }

    public pos: Victor;

    constructor(private xPos: number, private angle: number = 0, public r: number = 2) {
        this.pos = new Victor(xPos, Particle.randomInRange(-5, 5))
            .rotateBy(angle);
    }

    public update(): void {
        this.pos.x -= Particle.randomInRange(0, 1);
        this.pos.y += Particle.randomInRange(-5, 5);

        let angle = this.pos.horizontalAngle();
        angle = Math.min(Math.max(0, angle), (Math.PI / 6));
        const magnitude = this.pos.length();

        this.pos = new Victor(magnitude, 0)
            .rotateBy(angle);
    }

    public intersects(snowflake: Particle[]): boolean {
        let result: boolean = false;
        for (const s of snowflake) {
            const d = this.pos.distance(s.pos);
            if (d < this.r * 2) {
                result = true;
                break;
            }
        }
        return result;
    }

    public finished(): boolean {
        return (this.pos.x < 1);
    }
}
