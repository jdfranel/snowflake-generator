export class ViewElement {
    private static NO_NAMESPACE = '__nonamespaces__';
    public children: ViewElement[] = [];
    public attributes: any = {};

    public constructor(public name: string, public namespace: string | null = null) {

    }

    public appendChild(child: ViewElement) {
        this.children.push(child);
    }

    public setAttributeNS(namespace: string | null, key: string, value: string) {
        const namespaceKey = namespace != null ? namespace : ViewElement.NO_NAMESPACE;
        let attributesNS = this.attributes[namespaceKey];
        if (attributesNS === undefined) {
            attributesNS = {};
        }
        attributesNS[key] = value;
        this.attributes[namespaceKey] = attributesNS;
    }

    public setAttribute(key: string, value: string) {
        this.setAttributeNS(null, key, value);
    }

    public toDomElement(): Element {
        const rootElement = document.createElementNS(this.namespace, this.name);
        const attributeNamespaceKeys: string[] = Object.keys(this.attributes);
        if (attributeNamespaceKeys.length > 0) {
            attributeNamespaceKeys.forEach((namespace) => {
                const attributeKeys: string[] = Object.keys(this.attributes[namespace]);
                attributeKeys.forEach((key) => {
                    if (namespace === ViewElement.NO_NAMESPACE) {
                        rootElement.setAttribute(key, this.attributes[namespace][key]);
                    } else {
                        rootElement.setAttributeNS(namespace, key, this.attributes[namespace][key]);
                    }
                });
            });
        }
        this.children.forEach((child) => rootElement.appendChild(child.toDomElement()));
        return rootElement;
    }

    public toString(): string {
        let output: string = '<' + this.name;
        const attributeNamespaceKeys: string[] = Object.keys(this.attributes);
        if (attributeNamespaceKeys.length > 0) {
            attributeNamespaceKeys.forEach((namespace) => {
                const attributeKeys: string[] = Object.keys(this.attributes[namespace]);
                attributeKeys.forEach((key) => output += ` ${key}="${this.attributes[namespace][key]}"`);
            });
        }
        if (this.children.length > 0) {
            output += '>';
            output += this.children.map((child) => child.toString()).join();
            output += `</${this.name}>`;
        } else {
            output += '/>';
        }

        return output;
    }
}