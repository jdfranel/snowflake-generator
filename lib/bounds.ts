export class Bounds {
    public x: { min: number, max: number } = {
        max: 0,
        min: Number.MAX_VALUE,
    };
    public y: { min: number, max: number } = {
        max: 0,
        min: Number.MAX_VALUE,
    };
    public addPointToBounds(x: number, y: number) {
        if (x < this.x.min) {
            this.x.min = x;
        }
        if (y < this.y.min) {
            this.y.min = y;
        }
        if (x > this.x.max) {
            this.x.max = x;
        }
        if (y > this.y.max) {
            this.y.max = y;
        }
    }
    public addLengthToStart(horizontalLength: number, verticalLength: number) {
        this.x.min -= horizontalLength;
        this.y.min -= verticalLength;
    }
    public addLengthToEnd(horizontalLength: number, verticalLength: number) {
        this.x.max += horizontalLength;
        this.y.max += verticalLength;
    }
    public addLengthToCenter(horizontalLength: number, verticalLength: number) {
        this.addLengthToStart(horizontalLength / 2, verticalLength / 2);
        this.addLengthToEnd(horizontalLength / 2, verticalLength / 2);
    }

    public width(): number {
        return this.x.max - this.x.min;
    }
    public height(): number {
        return this.y.max - this.y.min;
    }
    public centerX(): number {
        return this.x.min + this.width();
    }
    public centerY(): number {
        return this.y.min + this.height();
    }
}