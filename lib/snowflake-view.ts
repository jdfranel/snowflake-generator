import { Bounds } from './bounds';
import { ParticleTreeNode } from './particle-tree-node';
import { Snowflake } from './snowflake';
import { ViewElement } from './view-element';

export class SnowflakeView {

    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    public static toSVGPath(snowflake: Snowflake, size?: number): ViewElement {
        const tree = snowflake.getParticleTree();
        const strokeWidth = (tree.root.value.r * 0.9);
        const path = new ViewElement('path', 'http://www.w3.org/2000/svg');
        path.setAttribute('stroke', 'black');
        path.setAttribute('stroke-width', strokeWidth.toString());
        path.setAttribute('stroke-linecap', 'round');
        path.setAttribute('stroke-linejoin', 'round');
        path.setAttribute('fill', 'none');
        path.setAttribute('d', 'M 0 0' + SnowflakeView.generateSVGNodePath(tree.root));

        const bounds = SnowflakeView.getParticlesBounds(snowflake);
        bounds.addLengthToCenter(strokeWidth * 2 * 1.2, strokeWidth * 2 * 1.2);
        const svg = SnowflakeView.generateSVGMarkup(
            path,
            bounds);
        if (size) {
            svg.setAttribute('width', size.toString());
            svg.setAttribute('height', size.toString());
        }
        return svg;
    }

    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    public static toSVGDots(snowflake: Snowflake, size?: number): ViewElement {
        const tree = snowflake.getParticleTree();
        const dotRadius = (tree.root.value.r * 0.9);
        const g = new ViewElement('g', 'http://www.w3.org/2000/svg');
        SnowflakeView.generateSVGNodeDots(tree.root, dotRadius).forEach((dot: ViewElement) => g.appendChild(dot));

        const bounds = SnowflakeView.getParticlesBounds(snowflake);
        bounds.addLengthToCenter(dotRadius * 2, dotRadius * 2);
        const svg = SnowflakeView.generateSVGMarkup(
            g,
            bounds);
        if (size) {
            svg.setAttribute('width', size.toString());
            svg.setAttribute('height', size.toString());
        }
        return svg;
    }

    private static getParticlesBounds(snowflake: Snowflake): Bounds {
        const bounds = new Bounds();
        snowflake.getParticles().forEach((particle) => {
            bounds.addPointToBounds(particle.pos.x, particle.pos.y);
        });
        return bounds;
    }

    private static generateSVGMarkup(halfBranchElement: ViewElement, bounds: Bounds): ViewElement {
        const svgNS = 'http://www.w3.org/2000/svg';
        const xlinkNS = 'http://www.w3.org/1999/xlink';

        const svg = new ViewElement('svg', svgNS);
        const defs = new  ViewElement('defs', svgNS);

        const viewBoxWidth = bounds.width() * 2;
        const viewBoxHeight = bounds.width() * 2;
        const viewBoxX = -bounds.width();
        const viewBoxY = -bounds.width();

        const flakeHalfBranchId = 'flake-half-branch-' + this.uuidv4();
        const flakeBranchId = 'flake-branch-' + this.uuidv4();

        svg.setAttribute('xmlns', svgNS);
        svg.setAttribute('xmlns:xlink', xlinkNS);
        svg.setAttribute('version', '1.1');
        svg.setAttribute('preserveAspectRatio', 'xMinYMax meet');
        svg.setAttribute('viewBox', viewBoxX + ' ' + viewBoxY + ' ' + viewBoxWidth + ' ' + viewBoxHeight);

        halfBranchElement.setAttribute('id', flakeHalfBranchId);

        defs.appendChild(halfBranchElement);
        svg.appendChild(defs);

        const branchGroup = new ViewElement('g', svgNS);
        branchGroup.setAttribute('id', flakeBranchId);

        const useFlakeHalfBranch = new ViewElement('use', svgNS);
        useFlakeHalfBranch.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeHalfBranchId);
        useFlakeHalfBranch.setAttribute('x', '0');
        useFlakeHalfBranch.setAttribute('y', '0');
        branchGroup.appendChild(useFlakeHalfBranch);

        const useFlakeHalfBranchSymetry = new ViewElement('use', svgNS);
        useFlakeHalfBranchSymetry.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeHalfBranchId);
        useFlakeHalfBranchSymetry.setAttribute('x', '0');
        useFlakeHalfBranchSymetry.setAttribute('y', '0');
        useFlakeHalfBranchSymetry.setAttribute('transform', 'scale(1 -1)');
        branchGroup.appendChild(useFlakeHalfBranchSymetry);

        defs.appendChild(branchGroup);

        const angle = 360 / 6;
        for (let i = 0; i < 6; i++) {
            const useFlakeBranch = new ViewElement('use', svgNS);
            useFlakeBranch.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeBranchId);
            useFlakeBranch.setAttribute('x', '0');
            useFlakeBranch.setAttribute('y', '0');
            useFlakeBranch.setAttribute('transform', 'rotate(' + (i * angle) + ')');
            useFlakeBranch.setAttribute('class', 'flake-branch');
            svg.appendChild(useFlakeBranch);
        }

        return svg;
    }

    private static generateSVGNodePath(node: ParticleTreeNode): string {
        let path: string = '';
        const xOffset: number = 0;
        const yOffset: number = 0;

        path += ' L ' + (node.value.pos.x - xOffset) + ' ' + (node.value.pos.y - yOffset);
        if (node.children.length === 0) {
            path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y - yOffset);
            path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y + yOffset);
        } else {
            node.children.forEach((child) => {
                path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y - yOffset);
                path += this.generateSVGNodePath(child);
                path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y + yOffset);
            });
        }
        path += ' L ' + (node.value.pos.x - xOffset) + ' ' + (node.value.pos.y + yOffset);

        return path;
    }

    private static generateSVGNodeDots(node: ParticleTreeNode, dotRadius: number): ViewElement[] {
        const svgNS = 'http://www.w3.org/2000/svg';
        const dots: ViewElement[] = [];

        const dot = new ViewElement('circle', svgNS);
        dot.setAttribute('cx', node.value.pos.x);
        dot.setAttribute('cy', node.value.pos.y);
        dot.setAttribute('r', dotRadius.toString());
        dots.push(dot);

        node.children.forEach((child) => {
            this.generateSVGNodeDots(child, dotRadius).forEach((childDot) => dots.push(childDot));
        });

        return dots;
    }

    private static uuidv4(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = Math.random() * 16 | 0;
            const v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}