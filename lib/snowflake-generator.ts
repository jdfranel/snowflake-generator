import { Snowflake } from './snowflake';

export class SnowflakeGenerator {
    /**
     * Generates a new completed snowflake
     * @param particleDistance
     * @returns A new completed snowflake
     */
    public static generate(particleDistance: number): Snowflake {
        const snowflake = SnowflakeGenerator.create(particleDistance);
        snowflake.generate();
        return snowflake;
    }

    public static create(particleDistance: number): Snowflake {
        const snowflake = new Snowflake(particleDistance);
        return snowflake;
    }
}