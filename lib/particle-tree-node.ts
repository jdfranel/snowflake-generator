import { Particle } from './particle';

export class ParticleTreeNode {

    public children: ParticleTreeNode[] = [];
    constructor(public value: any, public parent?: ParticleTreeNode) { }

    public addChild(node: ParticleTreeNode) {
        this.children.push(node);
    }

    public closestNodeForParticle(particle: Particle, previousBest?: ParticleTreeNode): ParticleTreeNode {
        const distance: number = particle.pos.distance(this.value.pos);
        let bestDistance: number = distance;
        let bestNode: ParticleTreeNode = this;

        if (previousBest) {
            const previousBestDistance = particle.pos.distance(previousBest.value.pos);
            if (previousBestDistance < distance) {
                bestDistance = previousBestDistance;
                bestNode = previousBest;
            }
        }

        this.children.forEach((child) => {
            const bestChild = child.closestNodeForParticle(particle, bestNode);
            if (bestChild) {
                const bestChildDistance = particle.pos.distance(bestChild.value.pos);
                if (bestChildDistance < bestDistance) {
                    bestNode = bestChild;
                    bestDistance = bestChildDistance;
                }
            }
        });

        return bestNode;
    }

    public depth(): number {
        let depth: number = 1;
        let maxChildDepth: number = 0;
        this.children.forEach((child) => {
            maxChildDepth = Math.max(maxChildDepth, child.depth());
        });
        depth += maxChildDepth;
        return depth;
    }

    public countChildren(): number {
        let count: number = 0;
        this.children.forEach((child) => count += child.countChildren());
        return count;
    }
}