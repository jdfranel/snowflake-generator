import { Snowflake } from './snowflake';
import { SnowflakeGenerator } from './snowflake-generator';

export function generate(particleDistance: number = 500): Snowflake {
    return SnowflakeGenerator.generate(particleDistance);
}

export function create(particleDistance: number = 500): Snowflake {
    return SnowflakeGenerator.create(particleDistance);
}