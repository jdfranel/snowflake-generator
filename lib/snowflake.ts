import { Bounds } from './bounds';
import { Particle } from './particle';
import { ParticleTree } from './particle-tree';
import { ParticleTreeNode } from './particle-tree-node';
import { SnowflakeView } from './snowflake-view';
import { ViewElement } from './view-element';

export class Snowflake {
    private particles: Particle[] = [];
    private done: boolean = false;

    /**
     * Creates a new snowflake to be generated with 1 particle.
     * @param distance The distance at witch particles will start to get to the snowflake
     */
    constructor(private distance: number = 500) {
        this.addParticle();
    }

    /**
     * Indicates if the snowflake is completed
     */
    public isDone(): boolean {
        return this.done;
    }

    /**
     * Generate the snowflake by using `addParticle()` until `isDone()`.
     */
    public generate(): void {
        while (!this.done) {
            const count = this.addParticle();
            if (count === 0) {
                this.done = true;
            }
        }
    }

    /**
     * Adds a patricle to the snowflake.
     * @returns The number of moves needed to make the particle meet the snowflake or -1 if
     * the snowflake is already completed
     */
    public addParticle(): number {
        if (!this.done) {
            const currentParticle: Particle = new Particle(this.distance);
            let count = 0;
            while (!currentParticle.finished() && !currentParticle.intersects(this.particles)) {
                currentParticle.update();
                count++;
            }
            this.particles.push(currentParticle);
            return count;
        } else {
            return -1;
        }
    }

    /**
     * Returns the particle array.
     */
    public getParticles(): Particle[] {
        return this.particles;
    }

    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    public toSVG(size: number = 100): Element | ViewElement {
        return this.toSVGPath(size);
    }

    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    public toSVGPath(size?: number): Element | ViewElement {
        return SnowflakeView.toSVGPath(this, size);
    }

    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    public toSVGDots(size?: number): Element | ViewElement {
        return SnowflakeView.toSVGDots(this, size);
    }

    public getParticleTree(): ParticleTree {
        let currentNode: ParticleTreeNode = new ParticleTreeNode(this.particles[0]);
        const tree = new ParticleTree(currentNode);

        if (this.particles.length === 1) { return tree; }

        for (let i = 1; i < this.particles.length; i++) {
            const particle = this.particles[i];
            const bestParent = tree.root.closestNodeForParticle(particle);
            currentNode = new ParticleTreeNode(particle, bestParent);
            bestParent.addChild(currentNode);
        }
        return tree;
    }
}