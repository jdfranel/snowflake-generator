import { ParticleTreeNode } from './particle-tree-node';

export class ParticleTree {
    constructor(public root: ParticleTreeNode) {
        this.root = root;
    }

    public depth() {
        return this.root.depth();
    }

    public countChildren() {
        return this.root.countChildren();
    }
}