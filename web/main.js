function updateParticlDistanceValue() {
    const particleDistance = document.getElementById('formControlRange').value;
    document.getElementById('formControlRangeValue').innerHTML = particleDistance;
}

updateParticlDistanceValue();

document.getElementById('formControlRange').onchange = function() {
    updateParticlDistanceValue();
}

let currentSnowUID = 0;
$('#snow-button').click((e) => {
    showWait();
    let delay = Math.floor(Math.random()*200);
    const air = document.getElementById('air');
    setTimeout(() => {
        clearAirSnowflakes();
        for (let i=0; i<10; i++) {
            let snowflake = snowflakeView(newCompleteSnowflake(100),document.body.offsetWidth/8);
            setTimeout(() => {
                air.appendChild(snowflake);
                snowing(snowflake,currentSnowUID);
            },delay);
            delay = Math.floor(Math.random()*1000);
        }
        hideWait();
        currentSnowUID++;
    },10);
});

function snowing(snowflake, snowUID) {
    let snowX = Math.floor(Math.random() * ($(air).width()+20))-10;

    let depth = Math.floor(Math.random() * 5);
    let snowSpd = Math.random() * 1500 * depth;
    let animLength = Math.floor(10000 - snowSpd);

    jQuery(snowflake).css({
        'left':snowX+'px',
        'filter':'blur('+(depth/2)+'px)',
        'z-index': 100+depth
    });
    if (snowUID === currentSnowUID) {
        jQuery(snowflake).animate({
            top: jQuery(air).height(),
            opacity : "0",
        }, animLength, function(){
            jQuery(this).css({
                'top':'-100px',
                'opacity':1,
                'filter':'blur(0px)',
                });
            snowing(this,snowUID);
        });
    }
}


const $generateButton = $('#generate-button');
const $newButton = $('#new-button');
const $stepButton = $('#step-button');
const $completeButton = $('#complete-button');
const $particleCountContainer = $('#particleCountContainer');
const $particleCount = $('#particleCount');

let currentSnowflake;
$generateButton.click((e) => {
    showWait();
    setTimeout(() => {
        const particleDistance = document.getElementById('formControlRange').value;
        currentSnowflake = newCompleteSnowflake(particleDistance);
        showCurrentSnowflake();
        updateCurrentParticleCount();
        $stepButton.prop("disabled",false);
        $completeButton.prop("disabled",false);
        $particleCountContainer.show();
        hideWait();
    },10);
});

$newButton.click((e) => {
    const particleDistance = document.getElementById('formControlRange').value;
    currentSnowflake = newSnowflake(particleDistance);
    showCurrentSnowflake();
    updateCurrentParticleCount();
    $stepButton.prop("disabled",false);
    $completeButton.prop("disabled",false);
    $particleCountContainer.show();
});

$stepButton.click((e) => {
    if (currentSnowflake) {
        currentSnowflake.addParticle();
        showCurrentSnowflake();
        updateCurrentParticleCount();
        if (currentSnowflake.isDone()) {
            $stepButton.prop("disabled",true);
            $completeButton.prop("disabled",true);
        }
    }
});

$completeButton.click((e) => {
    showWait();
    setTimeout(() => {
        if (currentSnowflake) {
            currentSnowflake.generate();
            showCurrentSnowflake();
            updateCurrentParticleCount();
            $stepButton.prop("disabled",true);
            $completeButton.prop("disabled",true);
            hideWait();
        }
    },10);
});

$('.wizard-toggle').click((e) => {
    $('#welcomecontainer').parents('.blockwrapper').toggleClass('side-left');
    $('#panelcontainer').parents('.blockwrapper').toggleClass('side-right');
});

$('#spinControl').on('change',(event) => {
    if (event.currentTarget.checked) {
        $('#snowflake-viewer').addClass('spin');
    } else {
        $('#snowflake-viewer').removeClass('spin');
    }
});

$('[name="displayMode"]').on('change',showCurrentSnowflake);

function snowflakeView(snowflake, size=undefined, mode='svg-path') {
    switch (mode) {
        case 'svg-dots':
            return snowflake.toSVGDots(size).toDomElement();
        case 'svg-path':
        default:
            return snowflake.toSVGPath(size).toDomElement();
    }
}

function clearSnowflakeViewer() {
    const snowflakeViewer = document.getElementById('snowflake-viewer');
    if (snowflakeViewer.hasChildNodes()) {
        for (let i=snowflakeViewer.childNodes.length-1; i>=0; i--) {
            snowflakeViewer.removeChild(snowflakeViewer.childNodes[i]);
        }
    }
}
function clearAirSnowflakes() {
    const air = document.getElementById('air');
    if (air.hasChildNodes()) {
        for (let i=air.childNodes.length-1; i>=0; i--) {
            air.removeChild(air.childNodes[i]);
        }
    }
}

function newSnowflake(particleDistance=500) {
    let snowflakeId = Math.floor(Math.random()*100000);

    const startTime = new Date().getTime();
    const snowflake = SnowflakeGenerator.create(particleDistance);
    const endTime = new Date().getTime();

    console.log("Snowflake "+snowflakeId+" has been generated in "+(endTime-startTime)/1000+"s and contains "+snowflake.getParticles().length+" particles.", snowflake);
    return snowflake;
}

function newCompleteSnowflake(particleDistance=500) {
    let snowflakeId = Math.floor(Math.random()*100000);

    const startTime = new Date().getTime();
    const snowflake = SnowflakeGenerator.generate(particleDistance);
    const endTime = new Date().getTime();

    console.log("Snowflake "+snowflakeId+" has been generated in "+(endTime-startTime)/1000+"s and contains "+snowflake.getParticles().length+" particles.", snowflake);
    return snowflake;
}

function showWait() {
    $('body').addClass('waiting');
}

function hideWait() {
    $('body').removeClass('waiting');
}

function showCurrentSnowflake() {
    if (currentSnowflake) {
        const snowflakeViewerContainer = document.getElementById('snowflake-viewer-container');
        const snowflakeDisplayMode = $('[name="displayMode"]:checked').val();
        const snowflakeViewer = document.getElementById('snowflake-viewer');
        const snowflakeSize = Math.min(snowflakeViewerContainer.offsetWidth,snowflakeViewerContainer.offsetHeight)*0.98;
        clearSnowflakeViewer();
        const view = snowflakeView(currentSnowflake,snowflakeSize,snowflakeDisplayMode)
        snowflakeViewer.appendChild(view);
    }
}

function updateCurrentParticleCount() {
    if (currentSnowflake) {
        let count = currentSnowflake.getParticles().length;
        $particleCount.html(count);
        if (count === 1) {
            $particleCountContainer.find('.plural').hide();
        } else {
            $particleCountContainer.find('.plural').show();
        }
    }
}
