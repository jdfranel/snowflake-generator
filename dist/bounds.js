"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Bounds = /** @class */ (function () {
    function Bounds() {
        this.x = {
            max: 0,
            min: Number.MAX_VALUE,
        };
        this.y = {
            max: 0,
            min: Number.MAX_VALUE,
        };
    }
    Bounds.prototype.addPointToBounds = function (x, y) {
        if (x < this.x.min) {
            this.x.min = x;
        }
        if (y < this.y.min) {
            this.y.min = y;
        }
        if (x > this.x.max) {
            this.x.max = x;
        }
        if (y > this.y.max) {
            this.y.max = y;
        }
    };
    Bounds.prototype.addLengthToStart = function (horizontalLength, verticalLength) {
        this.x.min -= horizontalLength;
        this.y.min -= verticalLength;
    };
    Bounds.prototype.addLengthToEnd = function (horizontalLength, verticalLength) {
        this.x.max += horizontalLength;
        this.y.max += verticalLength;
    };
    Bounds.prototype.addLengthToCenter = function (horizontalLength, verticalLength) {
        this.addLengthToStart(horizontalLength / 2, verticalLength / 2);
        this.addLengthToEnd(horizontalLength / 2, verticalLength / 2);
    };
    Bounds.prototype.width = function () {
        return this.x.max - this.x.min;
    };
    Bounds.prototype.height = function () {
        return this.y.max - this.y.min;
    };
    Bounds.prototype.centerX = function () {
        return this.x.min + this.width();
    };
    Bounds.prototype.centerY = function () {
        return this.y.min + this.height();
    };
    return Bounds;
}());
exports.Bounds = Bounds;
//# sourceMappingURL=bounds.js.map