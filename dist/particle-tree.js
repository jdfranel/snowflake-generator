"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParticleTree = /** @class */ (function () {
    function ParticleTree(root) {
        this.root = root;
        this.root = root;
    }
    ParticleTree.prototype.depth = function () {
        return this.root.depth();
    };
    ParticleTree.prototype.countChildren = function () {
        return this.root.countChildren();
    };
    return ParticleTree;
}());
exports.ParticleTree = ParticleTree;
//# sourceMappingURL=particle-tree.js.map