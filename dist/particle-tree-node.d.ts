import { Particle } from './particle';
export declare class ParticleTreeNode {
    value: any;
    parent?: ParticleTreeNode | undefined;
    children: ParticleTreeNode[];
    constructor(value: any, parent?: ParticleTreeNode | undefined);
    addChild(node: ParticleTreeNode): void;
    closestNodeForParticle(particle: Particle, previousBest?: ParticleTreeNode): ParticleTreeNode;
    depth(): number;
    countChildren(): number;
}
