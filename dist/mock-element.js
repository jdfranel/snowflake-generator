"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MockElement = /** @class */ (function () {
    function MockElement(name, namespace) {
        this.name = name;
        this.namespace = namespace;
        this.children = [];
        this.attributes = {};
    }
    MockElement.prototype.appendChild = function (child) {
        this.children.push(child);
    };
    MockElement.prototype.setAttributeNS = function (namespace, key, value) {
        this.setAttribute(key, value);
    };
    MockElement.prototype.setAttribute = function (key, value) {
        this.attributes[key] = value;
    };
    MockElement.prototype.toString = function () {
        return "<" + name + ">";
    };
    return MockElement;
}());
exports.MockElement = MockElement;
//# sourceMappingURL=mock-element.js.map