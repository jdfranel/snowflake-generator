import { ParticleTreeNode } from './particle-tree-node';
export declare class ParticleTree {
    root: ParticleTreeNode;
    constructor(root: ParticleTreeNode);
    depth(): number;
    countChildren(): number;
}
