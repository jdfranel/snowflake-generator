"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var snowflake_1 = require("./snowflake");
var SnowflakeGenerator = /** @class */ (function () {
    function SnowflakeGenerator() {
    }
    /**
     * Generates a new completed snowflake
     * @param particleDistance
     * @returns A new completed snowflake
     */
    SnowflakeGenerator.generate = function (particleDistance) {
        var snowflake = SnowflakeGenerator.create(particleDistance);
        snowflake.generate();
        return snowflake;
    };
    SnowflakeGenerator.create = function (particleDistance) {
        var snowflake = new snowflake_1.Snowflake(particleDistance);
        return snowflake;
    };
    return SnowflakeGenerator;
}());
exports.SnowflakeGenerator = SnowflakeGenerator;
//# sourceMappingURL=snowflake-generator.js.map