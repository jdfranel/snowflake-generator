import { Snowflake } from './snowflake';
export declare function generate(particleDistance?: number): Snowflake;
export declare function create(particleDistance?: number): Snowflake;
