export declare class ViewElement {
    name: string;
    namespace: string | null;
    private static NO_NAMESPACE;
    children: ViewElement[];
    attributes: any;
    constructor(name: string, namespace?: string | null);
    appendChild(child: ViewElement): void;
    setAttributeNS(namespace: string | null, key: string, value: string): void;
    setAttribute(key: string, value: string): void;
    toDomElement(): Element;
    toString(): string;
}
