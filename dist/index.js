"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var snowflake_generator_1 = require("./snowflake-generator");
function generate(particleDistance) {
    if (particleDistance === void 0) { particleDistance = 500; }
    return snowflake_generator_1.SnowflakeGenerator.generate(particleDistance);
}
exports.generate = generate;
function create(particleDistance) {
    if (particleDistance === void 0) { particleDistance = 500; }
    return snowflake_generator_1.SnowflakeGenerator.create(particleDistance);
}
exports.create = create;
//# sourceMappingURL=index.js.map