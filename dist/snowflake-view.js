"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bounds_1 = require("./bounds");
var view_element_1 = require("./view-element");
var SnowflakeView = /** @class */ (function () {
    function SnowflakeView() {
    }
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    SnowflakeView.toSVGPath = function (snowflake, size) {
        var tree = snowflake.getParticleTree();
        var strokeWidth = (tree.root.value.r * 0.9);
        var path = new view_element_1.ViewElement('path', 'http://www.w3.org/2000/svg');
        path.setAttribute('stroke', 'black');
        path.setAttribute('stroke-width', strokeWidth.toString());
        path.setAttribute('stroke-linecap', 'round');
        path.setAttribute('stroke-linejoin', 'round');
        path.setAttribute('fill', 'none');
        path.setAttribute('d', 'M 0 0' + SnowflakeView.generateSVGNodePath(tree.root));
        var bounds = SnowflakeView.getParticlesBounds(snowflake);
        bounds.addLengthToCenter(strokeWidth * 2 * 1.2, strokeWidth * 2 * 1.2);
        var svg = SnowflakeView.generateSVGMarkup(path, bounds);
        if (size) {
            svg.setAttribute('width', size.toString());
            svg.setAttribute('height', size.toString());
        }
        return svg;
    };
    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    SnowflakeView.toSVGDots = function (snowflake, size) {
        var tree = snowflake.getParticleTree();
        var dotRadius = (tree.root.value.r * 0.9);
        var g = new view_element_1.ViewElement('g', 'http://www.w3.org/2000/svg');
        SnowflakeView.generateSVGNodeDots(tree.root, dotRadius).forEach(function (dot) { return g.appendChild(dot); });
        var bounds = SnowflakeView.getParticlesBounds(snowflake);
        bounds.addLengthToCenter(dotRadius * 2, dotRadius * 2);
        var svg = SnowflakeView.generateSVGMarkup(g, bounds);
        if (size) {
            svg.setAttribute('width', size.toString());
            svg.setAttribute('height', size.toString());
        }
        return svg;
    };
    SnowflakeView.getParticlesBounds = function (snowflake) {
        var bounds = new bounds_1.Bounds();
        snowflake.getParticles().forEach(function (particle) {
            bounds.addPointToBounds(particle.pos.x, particle.pos.y);
        });
        return bounds;
    };
    SnowflakeView.generateSVGMarkup = function (halfBranchElement, bounds) {
        var svgNS = 'http://www.w3.org/2000/svg';
        var xlinkNS = 'http://www.w3.org/1999/xlink';
        var svg = new view_element_1.ViewElement('svg', svgNS);
        var defs = new view_element_1.ViewElement('defs', svgNS);
        var viewBoxWidth = bounds.width() * 2;
        var viewBoxHeight = bounds.width() * 2;
        var viewBoxX = -bounds.width();
        var viewBoxY = -bounds.width();
        var flakeHalfBranchId = 'flake-half-branch-' + this.uuidv4();
        var flakeBranchId = 'flake-branch-' + this.uuidv4();
        svg.setAttribute('xmlns', svgNS);
        svg.setAttribute('xmlns:xlink', xlinkNS);
        svg.setAttribute('version', '1.1');
        svg.setAttribute('preserveAspectRatio', 'xMinYMax meet');
        svg.setAttribute('viewBox', viewBoxX + ' ' + viewBoxY + ' ' + viewBoxWidth + ' ' + viewBoxHeight);
        halfBranchElement.setAttribute('id', flakeHalfBranchId);
        defs.appendChild(halfBranchElement);
        svg.appendChild(defs);
        var branchGroup = new view_element_1.ViewElement('g', svgNS);
        branchGroup.setAttribute('id', flakeBranchId);
        var useFlakeHalfBranch = new view_element_1.ViewElement('use', svgNS);
        useFlakeHalfBranch.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeHalfBranchId);
        useFlakeHalfBranch.setAttribute('x', '0');
        useFlakeHalfBranch.setAttribute('y', '0');
        branchGroup.appendChild(useFlakeHalfBranch);
        var useFlakeHalfBranchSymetry = new view_element_1.ViewElement('use', svgNS);
        useFlakeHalfBranchSymetry.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeHalfBranchId);
        useFlakeHalfBranchSymetry.setAttribute('x', '0');
        useFlakeHalfBranchSymetry.setAttribute('y', '0');
        useFlakeHalfBranchSymetry.setAttribute('transform', 'scale(1 -1)');
        branchGroup.appendChild(useFlakeHalfBranchSymetry);
        defs.appendChild(branchGroup);
        var angle = 360 / 6;
        for (var i = 0; i < 6; i++) {
            var useFlakeBranch = new view_element_1.ViewElement('use', svgNS);
            useFlakeBranch.setAttributeNS(xlinkNS, 'xlink:href', '#' + flakeBranchId);
            useFlakeBranch.setAttribute('x', '0');
            useFlakeBranch.setAttribute('y', '0');
            useFlakeBranch.setAttribute('transform', 'rotate(' + (i * angle) + ')');
            useFlakeBranch.setAttribute('class', 'flake-branch');
            svg.appendChild(useFlakeBranch);
        }
        return svg;
    };
    SnowflakeView.generateSVGNodePath = function (node) {
        var _this = this;
        var path = '';
        var xOffset = 0;
        var yOffset = 0;
        path += ' L ' + (node.value.pos.x - xOffset) + ' ' + (node.value.pos.y - yOffset);
        if (node.children.length === 0) {
            path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y - yOffset);
            path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y + yOffset);
        }
        else {
            node.children.forEach(function (child) {
                path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y - yOffset);
                path += _this.generateSVGNodePath(child);
                path += ' L ' + (node.value.pos.x + xOffset) + ' ' + (node.value.pos.y + yOffset);
            });
        }
        path += ' L ' + (node.value.pos.x - xOffset) + ' ' + (node.value.pos.y + yOffset);
        return path;
    };
    SnowflakeView.generateSVGNodeDots = function (node, dotRadius) {
        var _this = this;
        var svgNS = 'http://www.w3.org/2000/svg';
        var dots = [];
        var dot = new view_element_1.ViewElement('circle', svgNS);
        dot.setAttribute('cx', node.value.pos.x);
        dot.setAttribute('cy', node.value.pos.y);
        dot.setAttribute('r', dotRadius.toString());
        dots.push(dot);
        node.children.forEach(function (child) {
            _this.generateSVGNodeDots(child, dotRadius).forEach(function (childDot) { return dots.push(childDot); });
        });
        return dots;
    };
    SnowflakeView.uuidv4 = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0;
            var v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    return SnowflakeView;
}());
exports.SnowflakeView = SnowflakeView;
//# sourceMappingURL=snowflake-view.js.map