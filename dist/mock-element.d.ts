export declare class MockElement {
    name: string;
    namespace?: string | undefined;
    children: any[];
    attributes: any;
    constructor(name: string, namespace?: string | undefined);
    appendChild(child: any): void;
    setAttributeNS(namespace: string, key: string, value: string): void;
    setAttribute(key: string, value: string): void;
    toString(): string;
}
