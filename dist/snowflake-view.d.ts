import { Snowflake } from './snowflake';
import { ViewElement } from './view-element';
export declare class SnowflakeView {
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    static toSVGPath(snowflake: Snowflake, size?: number): ViewElement;
    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    static toSVGDots(snowflake: Snowflake, size?: number): ViewElement;
    private static getParticlesBounds;
    private static generateSVGMarkup;
    private static generateSVGNodePath;
    private static generateSVGNodeDots;
    private static uuidv4;
}
