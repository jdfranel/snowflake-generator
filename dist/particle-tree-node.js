"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParticleTreeNode = /** @class */ (function () {
    function ParticleTreeNode(value, parent) {
        this.value = value;
        this.parent = parent;
        this.children = [];
    }
    ParticleTreeNode.prototype.addChild = function (node) {
        this.children.push(node);
    };
    ParticleTreeNode.prototype.closestNodeForParticle = function (particle, previousBest) {
        var distance = particle.pos.distance(this.value.pos);
        var bestDistance = distance;
        var bestNode = this;
        if (previousBest) {
            var previousBestDistance = particle.pos.distance(previousBest.value.pos);
            if (previousBestDistance < distance) {
                bestDistance = previousBestDistance;
                bestNode = previousBest;
            }
        }
        this.children.forEach(function (child) {
            var bestChild = child.closestNodeForParticle(particle, bestNode);
            if (bestChild) {
                var bestChildDistance = particle.pos.distance(bestChild.value.pos);
                if (bestChildDistance < bestDistance) {
                    bestNode = bestChild;
                    bestDistance = bestChildDistance;
                }
            }
        });
        return bestNode;
    };
    ParticleTreeNode.prototype.depth = function () {
        var depth = 1;
        var maxChildDepth = 0;
        this.children.forEach(function (child) {
            maxChildDepth = Math.max(maxChildDepth, child.depth());
        });
        depth += maxChildDepth;
        return depth;
    };
    ParticleTreeNode.prototype.countChildren = function () {
        var count = 0;
        this.children.forEach(function (child) { return count += child.countChildren(); });
        return count;
    };
    return ParticleTreeNode;
}());
exports.ParticleTreeNode = ParticleTreeNode;
//# sourceMappingURL=particle-tree-node.js.map