import Victor = require('victor');
export declare class Particle {
    private xPos;
    private angle;
    r: number;
    private static randomInRange;
    pos: Victor;
    constructor(xPos: number, angle?: number, r?: number);
    update(): void;
    intersects(snowflake: Particle[]): boolean;
    finished(): boolean;
}
