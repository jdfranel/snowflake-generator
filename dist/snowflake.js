"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var particle_1 = require("./particle");
var particle_tree_1 = require("./particle-tree");
var particle_tree_node_1 = require("./particle-tree-node");
var snowflake_view_1 = require("./snowflake-view");
var Snowflake = /** @class */ (function () {
    /**
     * Creates a new snowflake to be generated with 1 particle.
     * @param distance The distance at witch particles will start to get to the snowflake
     */
    function Snowflake(distance) {
        if (distance === void 0) { distance = 500; }
        this.distance = distance;
        this.particles = [];
        this.done = false;
        this.addParticle();
    }
    /**
     * Indicates if the snowflake is completed
     */
    Snowflake.prototype.isDone = function () {
        return this.done;
    };
    /**
     * Generate the snowflake by using `addParticle()` until `isDone()`.
     */
    Snowflake.prototype.generate = function () {
        while (!this.done) {
            var count = this.addParticle();
            if (count === 0) {
                this.done = true;
            }
        }
    };
    /**
     * Adds a patricle to the snowflake.
     * @returns The number of moves needed to make the particle meet the snowflake or -1 if
     * the snowflake is already completed
     */
    Snowflake.prototype.addParticle = function () {
        if (!this.done) {
            var currentParticle = new particle_1.Particle(this.distance);
            var count = 0;
            while (!currentParticle.finished() && !currentParticle.intersects(this.particles)) {
                currentParticle.update();
                count++;
            }
            this.particles.push(currentParticle);
            return count;
        }
        else {
            return -1;
        }
    };
    /**
     * Returns the particle array.
     */
    Snowflake.prototype.getParticles = function () {
        return this.particles;
    };
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    Snowflake.prototype.toSVG = function (size) {
        if (size === void 0) { size = 100; }
        return this.toSVGPath(size);
    };
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    Snowflake.prototype.toSVGPath = function (size) {
        return snowflake_view_1.SnowflakeView.toSVGPath(this, size);
    };
    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    Snowflake.prototype.toSVGDots = function (size) {
        return snowflake_view_1.SnowflakeView.toSVGDots(this, size);
    };
    Snowflake.prototype.getParticleTree = function () {
        var currentNode = new particle_tree_node_1.ParticleTreeNode(this.particles[0]);
        var tree = new particle_tree_1.ParticleTree(currentNode);
        if (this.particles.length === 1) {
            return tree;
        }
        for (var i = 1; i < this.particles.length; i++) {
            var particle = this.particles[i];
            var bestParent = tree.root.closestNodeForParticle(particle);
            currentNode = new particle_tree_node_1.ParticleTreeNode(particle, bestParent);
            bestParent.addChild(currentNode);
        }
        return tree;
    };
    return Snowflake;
}());
exports.Snowflake = Snowflake;
//# sourceMappingURL=snowflake.js.map