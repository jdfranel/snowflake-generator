import { Particle } from './particle';
import { ParticleTree } from './particle-tree';
import { ViewElement } from './view-element';
export declare class Snowflake {
    private distance;
    private particles;
    private done;
    /**
     * Creates a new snowflake to be generated with 1 particle.
     * @param distance The distance at witch particles will start to get to the snowflake
     */
    constructor(distance?: number);
    /**
     * Indicates if the snowflake is completed
     */
    isDone(): boolean;
    /**
     * Generate the snowflake by using `addParticle()` until `isDone()`.
     */
    generate(): void;
    /**
     * Adds a patricle to the snowflake.
     * @returns The number of moves needed to make the particle meet the snowflake or -1 if
     * the snowflake is already completed
     */
    addParticle(): number;
    /**
     * Returns the particle array.
     */
    getParticles(): Particle[];
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    toSVG(size?: number): Element | ViewElement;
    /**
     * Generates an SVG path representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    toSVGPath(size?: number): Element | ViewElement;
    /**
     * Generates an SVG dots representation of the particles composing the snowflake.
     * @param size The width and height of the svg output
     */
    toSVGDots(size?: number): Element | ViewElement;
    getParticleTree(): ParticleTree;
}
