"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ViewElement = /** @class */ (function () {
    function ViewElement(name, namespace) {
        if (namespace === void 0) { namespace = null; }
        this.name = name;
        this.namespace = namespace;
        this.children = [];
        this.attributes = {};
    }
    ViewElement.prototype.appendChild = function (child) {
        this.children.push(child);
    };
    ViewElement.prototype.setAttributeNS = function (namespace, key, value) {
        var namespaceKey = namespace != null ? namespace : ViewElement.NO_NAMESPACE;
        var attributesNS = this.attributes[namespaceKey];
        if (attributesNS === undefined) {
            attributesNS = {};
        }
        attributesNS[key] = value;
        this.attributes[namespaceKey] = attributesNS;
    };
    ViewElement.prototype.setAttribute = function (key, value) {
        this.setAttributeNS(null, key, value);
    };
    ViewElement.prototype.toDomElement = function () {
        var _this = this;
        var rootElement = document.createElementNS(this.namespace, this.name);
        var attributeNamespaceKeys = Object.keys(this.attributes);
        if (attributeNamespaceKeys.length > 0) {
            attributeNamespaceKeys.forEach(function (namespace) {
                var attributeKeys = Object.keys(_this.attributes[namespace]);
                attributeKeys.forEach(function (key) {
                    if (namespace === ViewElement.NO_NAMESPACE) {
                        rootElement.setAttribute(key, _this.attributes[namespace][key]);
                    }
                    else {
                        rootElement.setAttributeNS(namespace, key, _this.attributes[namespace][key]);
                    }
                });
            });
        }
        this.children.forEach(function (child) { return rootElement.appendChild(child.toDomElement()); });
        return rootElement;
    };
    ViewElement.prototype.toString = function () {
        var _this = this;
        var output = '<' + this.name;
        var attributeNamespaceKeys = Object.keys(this.attributes);
        if (attributeNamespaceKeys.length > 0) {
            attributeNamespaceKeys.forEach(function (namespace) {
                var attributeKeys = Object.keys(_this.attributes[namespace]);
                attributeKeys.forEach(function (key) { return output += " " + key + "=\"" + _this.attributes[namespace][key] + "\""; });
            });
        }
        if (this.children.length > 0) {
            output += '>';
            output += this.children.map(function (child) { return child.toString(); }).join();
            output += "</" + this.name + ">";
        }
        else {
            output += '/>';
        }
        return output;
    };
    ViewElement.NO_NAMESPACE = '__nonamespaces__';
    return ViewElement;
}());
exports.ViewElement = ViewElement;
//# sourceMappingURL=view-element.js.map