export declare class Bounds {
    x: {
        min: number;
        max: number;
    };
    y: {
        min: number;
        max: number;
    };
    addPointToBounds(x: number, y: number): void;
    addLengthToStart(horizontalLength: number, verticalLength: number): void;
    addLengthToEnd(horizontalLength: number, verticalLength: number): void;
    addLengthToCenter(horizontalLength: number, verticalLength: number): void;
    width(): number;
    height(): number;
    centerX(): number;
    centerY(): number;
}
