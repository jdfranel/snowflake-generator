"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var view_element_1 = require("./view-element");
var ElementGenerator = /** @class */ (function () {
    function ElementGenerator() {
    }
    ElementGenerator.prototype.createElementNS = function (namespace, name) {
        if (document !== undefined) {
            return document.createElementNS(namespace, name);
        }
        return new view_element_1.ViewElement(name, namespace);
    };
    return ElementGenerator;
}());
exports.ElementGenerator = ElementGenerator;
//# sourceMappingURL=element-generator.js.map