import { Snowflake } from './snowflake';
export declare class SnowflakeGenerator {
    /**
     * Generates a new completed snowflake
     * @param particleDistance
     * @returns A new completed snowflake
     */
    static generate(particleDistance: number): Snowflake;
    static create(particleDistance: number): Snowflake;
}
