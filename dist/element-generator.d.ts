import { ViewElement } from "./view-element";
export declare class ElementGenerator {
    createElementNS(namespace: string, name: string): Element | ViewElement;
}
