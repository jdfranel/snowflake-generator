"use strict";
// Snowflake particle class. Original code from :
//
// Coding Challenge 127: Brownian Motion Snowflake
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/127-brownian-snowflake.html
// https://youtu.be/XUA8UREROYE
Object.defineProperty(exports, "__esModule", { value: true });
var Victor = require("victor");
var Particle = /** @class */ (function () {
    function Particle(xPos, angle, r) {
        if (angle === void 0) { angle = 0; }
        if (r === void 0) { r = 2; }
        this.xPos = xPos;
        this.angle = angle;
        this.r = r;
        this.pos = new Victor(xPos, Particle.randomInRange(-5, 5))
            .rotateBy(angle);
    }
    Particle.randomInRange = function (min, max) {
        var diff = max - min;
        return min + (Math.random() * diff);
    };
    Particle.prototype.update = function () {
        this.pos.x -= Particle.randomInRange(0, 1);
        this.pos.y += Particle.randomInRange(-5, 5);
        var angle = this.pos.horizontalAngle();
        angle = Math.min(Math.max(0, angle), (Math.PI / 6));
        var magnitude = this.pos.length();
        this.pos = new Victor(magnitude, 0)
            .rotateBy(angle);
    };
    Particle.prototype.intersects = function (snowflake) {
        var result = false;
        for (var _i = 0, snowflake_1 = snowflake; _i < snowflake_1.length; _i++) {
            var s = snowflake_1[_i];
            var d = this.pos.distance(s.pos);
            if (d < this.r * 2) {
                result = true;
                break;
            }
        }
        return result;
    };
    Particle.prototype.finished = function () {
        return (this.pos.x < 1);
    };
    return Particle;
}());
exports.Particle = Particle;
//# sourceMappingURL=particle.js.map