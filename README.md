Snowflake Generator
=============================

A Snowflake generator using Bronian tree method. From a snowflake you can create an SVG view using a path or multiple points.

This implementation is based on Daniel Shiffman's [Coding Challenge 127: Brownian Motion Snowflake](https://youtu.be/XUA8UREROYE) impementation using p5.js.

It works in both Node.js and the browser.

More informations on the drawing of a snowflake on this [code challenge - Draw a Snowflake](https://codegolf.stackexchange.com/questions/42506/draw-a-snowflake) page.

## Installation

### Node.js

```bash
npm install @jdfranel/snowflake-generator --save
```

```javascript
const SnowflakeGenerator = require('@jdfranel/snowflake-generator');
var snowflake = SnowflakeGenerator.create();
```

### Global object

Include the pre-built script.

```html
<script src="./build/snowflake-generator.js"></script>
<script>
var snowflake = SnowflakeGenerator.create();
</script>
```

## Build

```bash
npm run build
```

## Documentation

### SnowflakeGenerator

```javascript
public static create(particleDistance: number): Snowflake
```

Returns an initiated snowflake with 1 particle. This snowflake can have more particle until the `particleDistance` is reached.

`particleDistance`: The distance at witch particles will start to get to the snowflake

```javascript
public static generate(particleDistance: number): Snowflake
```

Returns a fully grown snowflake using `snowflake.generate()`. Depending of the `particleDistance` the generation of a snowflake can take some time (around 1.3s for a distance of 500).

`particleDistance`: The distance at witch particles will start to get to the snowflake

### Snowflake

```javascript
constructor(distance: number = 500)
```

Creates a new snowflake to be generated with 1 particle.
`distance`: The distance at witch particles will start to get to the snowflake

```javascript
public isDone(): boolean
```

Indicates if the snowflake is completed.

```javascript
public generate(): void
```

Generate the snowflake by using `addParticle()` until `isDone()`.

```javascript
public addParticle(): number
```

Adds a patricle to the snowflake.
Returns The number of moves needed to make the particle meet the snowflake or `-1` if the snowflake is already completed.

```javascript
public getParticles(): Particle[]
```

Returns the particle array.

```javascript
public toSVG(size: number = 100): ViewElement
```

Generates an SVG path representation of the particles composing the snowflake.
`size`: The width and height of the svg output.

```javascript
public toSVGPath(size?: number): ViewElement
```

Generates an SVG path representation of the particles composing the snowflake.
`size`: The width and height of the svg output.

```javascript
public toSVGDots(size?: number): ViewElement
```

Generates an SVG dots representation of the particles composing the snowflake.
`size`: The width and height of the svg output

### ViewElement

The `ViewElement` class exists to allow the library to generate the SVG structure and convert it as a platform compatible format.

```javascript
public toDomElement(): Element
```

Generates an SVG DOM Element that you can append into your document when **using Snowflake generator in a browser**.

```javascript
public toString(): string
```

Generates an string representation of the SVG Element that you can print, save or do whatever you need when **using Snowflake generator as a nodejs module**.

### Particle

The `Particle` class is not of much interest to generate the snowflake but you can tweek the `update()` function to fit your likings.

Most of the time you will want to get the particle position using the following:

```javascript
particle.pos: Victor
```

The position of the particle is stored in a Victor.js 2d vector.

## Demo

You can find a single web page demo that show how you can manipulate a snowflake and render it.

The demo can be found in the `./web` folder.

## Credits

- [Daniel Shiffman](https://shiffman.net/) : Inspiration and implementation in p5.js in [Coding Challenge 127: Brownian Motion Snowflake](https://youtu.be/XUA8UREROYE)
- [Jean-Daniel Franel](mailto:jd@franel.me) : The rest.

## License

MIT

## Donate

In case you want to compensate all the snowy fun you have by a beer, you will need this.

- BTC : 37kyGHUfAh7zJ3znaBNhspgHMXddimztox
- ETH : 0x705C45Dcf60EFc7aF6521B4211940dBB7d25aFF5
